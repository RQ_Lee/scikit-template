# Author: Lee Rhui Dih
# Matric: U1420374E

import matplotlib.pyplot as plt
from sklearn import mixture 
from sklearn import cluster
import pandas as pd
import numpy as np
import json

# environment variables
cuisine_dir = 'assign3_CuisineData.json'
response_dir = 'responses.csv'

##############################################################################
##### Problem 1: Cuisin Data Clustering 	##################################
##############################################################################
print("\n<<<Problem 1: Cuisin Data Clustering>>>>>>>>>>>>>")


# helper function ############################################################
ingredientDict = dict()
def processString(list_string):
	''' remove bracket content '''

	for i in range(len(list_string)):
		clean = list_string[i].split(',', 1)[0]
		if (clean[0] == '('):
			clean = clean.split(')', 1)[1]
		else:
			clean = clean.split('(', 1)[0]
		list_string[i] = clean.lower().split()

	return recordString([i for j in list_string for i in j])



# helper function ############################################################
def recordString(list_string):
	''' record frequency of each ingredient '''

	for i in list_string:
		if not(i in ingredientDict.keys()):
			ingredientDict.update({i:1})
		else:
			ingredientDict[i] += 1

	return list_string



# read data and clean the content string #####################################
with open(cuisine_dir,'r') as f:
	trainJson = f.read()
trainDict = json.loads(trainJson)
trainList = [[i['id'], processString(i['ingredients'])] for i in trainDict]



# exploratory data analyst ###################################################
print("[Exploratory Analysis]")
total_dish = len(trainList)
ingredientList = list(ingredientDict.keys())
ingredientCount = [[i, ingredientDict[i]] for i in ingredientList]
ingredientCount.sort(key = lambda x : x[1], reverse=True)
total_ingredient = len(ingredientList)
most_used_ingredient = ingredientCount[0][0]
#least_used_ingredient = ingredientCount[-1026][0]
dish_most_ingredient = max([len(i[1]) for i in trainList])
dish_least_ingredient = min([len(i[1]) for i in trainList])
freq_10, freq_100, freq_1k, freq_10k = 0, 0, 0, 0
for i in ingredientCount:
	if (i[1]>10e3):
		freq_10k += 1
	elif (i[1]>10e2):
		freq_1k += 1
	elif (i[1]>=10):
		freq_100 += 1
	else:
		freq_10 += 1

print("examples #:\t{0}".format(total_dish))
print("features #:\t{0}".format(total_ingredient))
print("highest freq:\t{0}".format(most_used_ingredient))
#print("lowest freq:\t{0}".format(least_used_ingredient))
print("most ingredients used:\t{0}".format(dish_most_ingredient))
print("least ingredients used:\t{0}".format(dish_least_ingredient))
print("number of ingredient classes:")
print("  freq<10:\t{0}".format(freq_10))
print("  10<freq<100:\t{0}".format(freq_100))
print("  100<freq<1k:\t{0}".format(freq_1k))
print("  1k<freq:\t{0}".format(freq_10k))



# filtering ###################################################################
# (keep ingredients with at least frequency 1k)

to_keep = freq_1k + freq_10k
train_ingredient = [i[0] for i in ingredientCount[:to_keep]]

print("\n*** drop ingredients with frequency < 10.")
print("*** number of ingredients used for clustering: {0}".format(total_ingredient-freq_10))



# encode string into integer representation: 0/1 embedding ###################
trainID = np.array([i[0] for i in trainList])
trainFeatures = np.array([[0]*len(train_ingredient)]*total_dish)

for i in range(len(trainFeatures)):
	for j in trainList[i][1]:
		if (j in train_ingredient):
			trainFeatures[i][train_ingredient.index(j)] = 1



# k-means clustering #########################################################
print('\n[K-Mean with Euclidean Clustering]')

''' best result is 7 cluster '''
n_cluster = 7
kmean = cluster.KMeans(n_cluster)
kmean.fit(trainFeatures)
print("best number of cluster used:\t{0}".format(n_cluster))
print("scores of training examples:\t{0:.2f}".format(kmean.score(trainFeatures)))

# Uncomment to use k-mean clustering from 2 - 20 cluster and plot the score results.
# Used for search the best cluster. Take long times to run.

'''
k = []
scores = []
for i in range(2, 20):
    kmean = cluster.KMeans(i)
    kmean.fit(trainFeatures)
    scores.append(kmean.score(trainFeatures))
    k.append(i)
print(k)
print(scores)
plt.figure()
plt.plot(k, scores, 'b.-')
plt.show()
'''

# Clustering Result #########################################################
print('\n[Clustering Result]')

labels = kmean.predict(trainFeatures)
labels_df = pd.DataFrame(labels, columns=['cluster'])

# summary of each cluster statistic
_, counts = np.unique(labels, return_counts=True)
for i in range(len(counts)):
    print("cluster {0}: {1:.2f}%".format(i+1, counts[i]/sum(counts)*100))


##############################################################################
##### Problem 2: Young People Survey Response Clustering 	##################
##############################################################################
print("\n\n<<<Problem 2: Young People Survey Response Clustering>>>>>>>>>>>>>")

# read data ##################################################################
responseData = pd.read_csv(response_dir)
responseData_fillna = responseData.fillna(responseData.mean())
responseData_dropna = responseData_fillna.dropna()
responseData_dummies = pd.get_dummies(responseData_dropna).reset_index().drop('index', axis=1)


# exploratory data analyst ###################################################
print("[Exploratory Analysis]")
total_response = len(responseData)
responseCriteria = list(responseData.columns.values)

print("examples #:\t{0}".format(total_response))
print("features #:\t{0}".format(len(responseCriteria)))


# k-means clustering #########################################################
print('\n[K-Mean with Euclidean Clustering]')
print('*take ~30 seconds to run')
trainFeatures = responseData_dummies.values

''' best result is 10 cluster '''
n_cluster = 9
kmean = cluster.KMeans(n_cluster)
kmean.fit(trainFeatures)
print("best number of cluster used:\t{0}".format(n_cluster))
print("scores of training examples:\t{0:.2f}".format(kmean.score(trainFeatures)))


# Uncomment to use k-mean clustering from 2 - 50 clusters and plot the score results.
# Used for search the best cluster. Take long times to run.
'''
k = []
scores = []
for i in range(2, 50):
    kmean = cluster.KMeans(i)
    kmean.fit(trainFeatures)
    scores.append(kmean.score(trainFeatures))
    k.append(i)
print(k)
print(scores)
plt.figure()
plt.plot(k, scores, 'b.-')
plt.show()
'''


# Clustering Result #########################################################
print('\n[Clustering Result]')

labels = kmean.predict(trainFeatures)
labels_df = pd.DataFrame(labels, columns=['cluster'])

# summary of each cluster statistic
_, counts = np.unique(labels, return_counts=True)
for i in range(len(counts)):
    print("cluster {0}: {1:.2f}%".format(i+1, counts[i]/sum(counts)*100))

# correlation
corr = pd.concat([responseData_dummies, labels_df], axis=1).corr()['cluster'][:-1]
most_important_variable = corr.idxmax()

print("\nMost important variable:\t{0}".format(most_important_variable))

# the highest correlation variable to the cluster classes is "Gender".
# hence we can say it plays important role in the clustering