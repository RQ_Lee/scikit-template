# Author: Lee Rhui Dih
# Matric: U1420374E
#
# Run this python file to have an overall view of the 2 solutions.
# View Description (in sequence):
# [Analysis]		- distribution plot & mean, std, max, min, 25%, 50%, 75%
# [Correlation]
# [Residuals]
# [RSE & Variance]
# [Significance]
#
# PyPlot is used to show distribution of each variables.
# Select [ZOOM BUTTON] and [DRAW & INCLUDE] particular box to have a
# better scaled view.
#
# Insight/Conclusion is written at end of each problem's code section below.

import pandas as pd  
from sklearn import linear_model
import matplotlib.pyplot as plt
import math

##### Environment Variables
WineData_dir = "./assign1_WineData.csv"
CarData_dir = "./assign1_CarData.csv"

##### Read CSV Files
WineData = pd.read_csv(WineData_dir)
CarData = pd.read_csv(CarData_dir)

##############################################################################
##### Problem 1: Predict Wine's Quality ######################################
##############################################################################
print("\n<<<Problem 1: Predict Wine's Quality>>>")

# descriptive data analysis							# run file to see distribution plot
print("examples #:\t{0}".format(WineData.shape[0]))
print("variables #:\t{0}".format(WineData.shape[1]))
#print(WineData.describe())							# un-comment to see in number
plt.figure("Problem1: Wine's Quality Prediction - 1")
WineData[WineData.columns[:6]].boxplot()
plt.figure("Problem1: Wine's Quality Prediction - 2")
WineData[WineData.columns[6:]].boxplot()

# calculate correlation against quality
corr = WineData.corr()['Quality']
FixedAcidity = corr[0]		# 0.090384
VolatileAcidity = corr[1]	# -0.392845
CitricAcid = corr[2]		# 0.208060
ResidualSugar = corr[3]		# 0.004935
Chlorides = corr[4]			# -0.130895
FreeSulphurDioxide = corr[5]	# -0.041890
TotalSulphurDioxide = corr[6]	# -0.184137
Density = corr[7]		# -0.196871
pH = corr[8]			# -0.045217
Sulphates = corr[9]		# 0.227164
Alcohol = corr[10]		# 0.468355
Quality = corr[11]		# 1.000000

# fit linear regression model
features = WineData.drop(['Quality'], axis=1)		# feature variables
quality  = WineData['Quality']						# target variable
lm = linear_model.LinearRegression()				# initialization
lm.fit(features, quality)

# calculate all residuals
predict = lm.predict(features)						# estimated quality
residuals = (quality-predict).describe()			# actual quality - estimated quality
mean = residuals['mean']	# mean = -2.18847e-15
stdd = residuals['std']		# std = 6.54759e-01
_min = residuals['min']		# min = -2.64329e+00
_max = residuals['max']		# max = 2.04637e+00
_25 = residuals['25%']		# 25% = -3.590248e-01
_50 = residuals['50%']		# 50% = -5.608005e-02
_75 = residuals['75%']		# 75% = 4.618094e-01

# calculate TSS = 656.87102
mean = quality.sum()/quality.count()
TSS = ((quality - mean) * (quality - mean)).sum()

# calculate RSS = 428.28186
predict = lm.predict(features)
RSS = ((quality - predict) * (quality - predict)).sum()

# calculate RSE = 6.58394e-01
n = features.shape[0]		# number of examples
p = features.shape[1]		# number of features
RSE = math.sqrt(RSS/(n - p - 1))

# calculate Variance R^2 = 3.40737e-01
VAR = (TSS/(n-1) - RSS/(n-p-1)) / (TSS/(n-1))

# getting significance of each variables
variables = features.columns
coef = lm.coef_

# coefficient of each variables
FixedAcidity = coef[0]		# 0.01006065573754673
VolatileAcidity = coef[1]	# -1.0751913444648304
CitricAcid = coef[2]		# -0.07450897998640932
ResidualSugar = coef[3]		# 0.018425475296879726
Chlorides = coef[4]			# -1.955027792283814
FreeSulphurDioxide = coef[5]	# 0.00480615091856651
TotalSulphurDioxide = coef[6]	# -0.003107429193878186
Density = coef[7]			# -20.210909763933856
pH      = coef[8]			# -0.4395290840383977
Sulphates = coef[9]			# 0.8155388088694859
Alcohol = coef[10]			# 0.2667793560405698

# conclusion/insight
# There are couples of variables have a low correlation with wine's quality,
# which are FixedAcidity, ResidualSugar, FreeSulyphurDioxide and pH. Assume
# these variables are conditionally indepent to Quality, removing these variables
# will increase the linear model's performance. Linear model shows density has
# a very large significance to the Quality.
# 
# Confidence of the linear model is only decent based on variance R^2, which
# is around 0.35. Hence, we can conclude the finding above could be noisy.

# print scripts
print("\n[Correlation]")
print(corr)
print("\n[Residuals]")
print("mean:\t{0}".format(mean))
print("std:\t{0}".format(stdd))
print("min:\t{0}".format(_min))
print("max:\t{0}".format(_max))
print("25%:\t{0}".format(_25))
print("50%:\t{0}".format(_50))
print("75%:\t{0}".format(_75))
print("\n[RSE & Variance]")
print("TSS:\t{0}".format(TSS))
print("RSS:\t{0}".format(RSS))
print("RSE:\t{0}".format(RSE))
print("VAR:\t{0}".format(VAR))
print("\n[Significance]")
for i in range(len(variables)):
	print("{0}:\t{1}".format(features.columns[i], coef[i]))


##############################################################################
##### Problem 2: Predict Car's Fuel Efficiency ###############################
##############################################################################
print("\n<<<Problem 2: Predict Car's Fuel Efficiency>>>")

# descriptive data analysis							# run file to see distribution plot
print("examples #:\t{0}".format(CarData.shape[0]))
print("variables #:\t{0}".format(CarData.shape[1]))
#print(WineData.describe())							# un-comment to see in number
plt.figure("Problem2: Car's Fuel Efficiency Prediction")
CarData.boxplot()

# calculate correlation against mpg
corr = CarData.corr()['mpg']
cylinders = corr[0]			# -0.785283
displacement = corr[1]		# -0.812838
horsepower = corr[2]		# -0.777009
weight = corr[3]			# -0.839080
acceleration = corr[4]		# 0.433420
mpg = corr[5]				# 1.000000

# fit linear regression model
features = CarData.drop(['mpg'], axis=1)			# feature variables
mpg  = CarData['mpg']								# target variable
lm = linear_model.LinearRegression()				# initialization
lm.fit(features, mpg)

# calculate all residuals
predict = lm.predict(features)						# estimated mpg
residuals = (mpg-predict).describe()				# actual mpg - estimated mpg
mean = residuals['mean']	# mean = -1.95399e-15
stdd = residuals['std']		# std = 4.23744
_min = residuals['min']		# min = -11.80628
_max = residuals['max']		# max = 16.30554
_25 = residuals['25%']		# 25% = -2.86438
_50 = residuals['50%']		# 50% = -4.43354e-01
_75 = residuals['75%']		# 75% = 2.36517

# calculate TSS = 18942.51346
mean = mpg.sum()/mpg.count()
TSS = ((mpg - mean) * (mpg - mean)).sum()

# calculate RSS = 5368.83014
predict = lm.predict(features)
RSS = ((mpg - predict) * (mpg - predict)).sum()

# calculate RSE = 4.27332
n = features.shape[0]		# number of examples
p = features.shape[1]		# number of features
RSE = math.sqrt(RSS/(n - p - 1))

# calculate Variance R^2 = 7.11752e-01
VAR = (TSS/(n-1) - RSS/(n-p-1)) / (TSS/(n-1))

# getting significance of each variables
variables = features.columns
coef = lm.coef_

# coefficient of each variables
cylinders = coef[0]			# -0.22752819289599838
displacement = coef[1]		# -0.006306739545686888
horsepower = coef[2]		# -0.03584699934237988
weight = coef[3]			# -0.005205904278729851
acceleration = coef[4]		# -0.03464693339141044

# conclusion/insight
# Linear model shows all variable contributes inversely to car's fuel efficiency.
# Whereas naive correlation shows acceleration contributes proportionally.
# No description of the acceleration variable could be found, hence no evidence
# can be used to prove its real relation. Assume it is accleration speed of a 
# car, it should be contributing proportionally to fuel efficiency, or otherwise.
#
# Confidence of the linear model is rather high based on variance R^2, which
# is around 0.7.

# print scripts
print("\n[Correlation]")
print(corr)
print("\n[Residuals]")
print("mean:\t{0}".format(mean))
print("std:\t{0}".format(stdd))
print("min:\t{0}".format(_min))
print("max:\t{0}".format(_max))
print("25%:\t{0}".format(_25))
print("50%:\t{0}".format(_50))
print("75%:\t{0}".format(_75))
print("\n[RSE & Variance]")
print("TSS:\t{0}".format(TSS))
print("RSS:\t{0}".format(RSS))
print("RSE:\t{0}".format(RSE))
print("VAR:\t{0}".format(VAR))
print("\n[Significance]")
for i in range(len(variables)):
	print("{0}:\t{1}".format(features.columns[i], coef[i]))

# show plot figure
plt.show()