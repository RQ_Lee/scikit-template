# Author: Lee Rhui Dih
# Matric: U1420374E
#
# Results:
"""
<<<Problem 1: Predict Churn's Quality>>>
examples #:     4991
variables #:    20
variables #(one-hot):   46
Most Important variable: Contract_Month-to-month

[Random Forest]
Training accuracy:      0.9805650170306551
5-Fold CV accuracy:     [ 0.7977978   0.78778779  0.78578579  0.78435306  0.75827482]
Average CV accuracy:    0.7827998510044648

[Optimal Random Forest]
Training accuracy:      0.8513323983169705
5-Fold CV accuracy:     [ 0.7997998   0.7977978   0.78378378  0.80341023  0.77432297]
Average CV accuracy:    0.7918229161960355
"""

"""
<<<Problem 2: Titanic Survival Prediction>>>
examples #:     183
variables #:    12
variables #(one-hot):   270
Most Important variable: Sex_female

[Random Forest]
Training accuracy:      0.994535519125683
5-Fold CV accuracy:     [ 0.78378378  0.75675676  0.72972973  0.63888889  0.75      ]
Average CV accuracy:    0.7318318318318318

[Optimal Random Forest]
Training accuracy:      0.9890710382513661
5-Fold CV accuracy:     [ 0.78378378  0.75675676  0.7027027   0.69444444  0.80555556]
Average CV accuracy:    0.7486486486486486
"""

# Accuracy subjects to change every run.
# Code implementation:

from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt
import pandas as pd
import math

##### Environment Variables
ChurnData_dir = "./assign2_ChurnData.csv"
TitanicTrain_dir = "./train.csv"
TitanicTest_dir = "./test.csv"

##### Read CSV Files
ChurnData = pd.read_csv(ChurnData_dir)
ChurnData = ChurnData.dropna()				# drop rows with any empty value
TitanicTrain = pd.read_csv(TitanicTrain_dir)
TitanicTrain = TitanicTrain.dropna()
TitanicTest = pd.read_csv(TitanicTest_dir)

##############################################################################
##### Problem 1: Predict Churn's Quality 	##################################
##############################################################################
print("\n<<<Problem 1: Predict Churn's Quality>>>")

# descriptive data analysis
print("examples #:\t{0}".format(ChurnData.shape[0]))
print("variables #:\t{0}".format(ChurnData.shape[1]))
features = ChurnData.drop(['Churn'], axis=1)
features_dummies = pd.get_dummies(features)
print("variables #(one-hot):\t{0}".format(features_dummies.shape[1]))
features_dummies_cat = features_dummies.drop(['Tenure','MonthlyCharges','TotalCharges'], axis=1)
features_dummies_num = features_dummies[['Tenure','MonthlyCharges','TotalCharges']]
churn = ChurnData['Churn']

# calculate correlation against churn quality
# set thres_drop to drop variables with correlation with set threshold
corr = pd.concat([features_dummies, pd.get_dummies(churn)], axis=1).corr()[['Yes']]
thres_drop = 0.0
for i in range(len(corr)):
	if(corr.iloc[i].values<thres_drop and corr.iloc[i].values>-thres_drop):
		features_dummies  = features_dummies.drop([corr.iloc[i].name], axis=1)
corr = pd.concat([features_dummies, pd.get_dummies(churn)], axis=1).corr()[['Yes']]
print("Most Important variable: {0}".format(corr[:-2].idxmax()[0]))

#print(corr[:-2])		# uncomment to see all correlation
# The most important variable is Contract_Month-to-month as it has the highest correlation

# build a build random forest on the dataset
print("\n[Random Forest]")
clf_randomforest = RandomForestClassifier(n_estimators = 10)
clf_randomforest.fit(features_dummies, churn)
print("Training accuracy:\t{0}".format(clf_randomforest.score(features_dummies, churn)))
score = cross_val_score(clf_randomforest, features_dummies, churn, cv=5)
print("5-Fold CV accuracy:\t{0}".format(score))
print("Average CV accuracy:\t{0}".format(sum(score)/len(score)))

# grid search the best parameter for tree (manual pruning)
print("\n[Optimal Random Forest]")
print("***take times to run")
parameters = {'max_features':('auto', 'sqrt', 2),
				'max_depth':[i for i in range(10, int(features_dummies.shape[1]/2))],
				'min_samples_split':[i for i in range(4,10)],}
clf = DecisionTreeClassifier(random_state=0)
clf = GridSearchCV(clf, parameters, cv=5)
clf.fit(features_dummies, churn)

clf_randomforest = RandomForestClassifier(n_estimators = 10)
clf_randomforest.set_params(max_features = clf.best_params_['max_features'],
							max_depth = clf.best_params_['max_depth'],
							min_samples_split = clf.best_params_['min_samples_split'])
clf_randomforest.fit(features_dummies, churn)

print("Training accuracy:\t{0}".format(clf_randomforest.score(features_dummies, churn)))
score = cross_val_score(clf_randomforest, features_dummies, churn, cv=5)
print("5-Fold CV accuracy:\t{0}".format(score))
print("Average CV accuracy:\t{0}".format(sum(score)/len(score)))

# export graph
export_graphviz(clf_randomforest.estimators_[0], out_file='problem1.dot',
					feature_names = features_dummies.columns.values,
					class_names = 'Churn')


##############################################################################
##### Problem 2: Titanic Survival Prediction 	##############################
##############################################################################
print("\n<<<Problem 2: Titanic Survival Prediction>>>")

# descriptive data analysis
print("examples #:\t{0}".format(TitanicTrain.shape[0]))
print("variables #:\t{0}".format(TitanicTrain.shape[1]))

# drop non-factor variables
features = TitanicTrain.drop(['Survived', 'PassengerId', 'Name'], axis=1)
features_dummies = pd.get_dummies(features)
print("variables #(one-hot):\t{0}".format(features_dummies.shape[1]))
features_dummies_cat = features_dummies.drop(['Age','SibSp','Parch','Fare'], axis=1)
features_dummies_num = features_dummies[['Age','SibSp','Parch','Fare']]
survived = TitanicTrain['Survived']

# calculate correlation against survived
corr = pd.concat([features_dummies, survived], axis=1).corr()[['Survived']]
print("Most Important variable: {0}".format(corr[:-1].idxmax()[0]))

#print(corr) # uncomment to see all correlation
# The most important variable is Contract_Month-to-month as it has the highest correlation

# build a build random forest on the dataset
print("\n[Random Forest]")
clf_randomforest = RandomForestClassifier(n_estimators = 10)
clf_randomforest.fit(features_dummies, survived)
print("Training accuracy:\t{0}".format(clf_randomforest.score(features_dummies, survived)))
score = cross_val_score(clf_randomforest, features_dummies, survived, cv=5)
print("5-Fold CV accuracy:\t{0}".format(score))
print("Average CV accuracy:\t{0}".format(sum(score)/len(score)))


# grid search the best parameter for tree (manual pruning)
print("\n[Optimal Random Forest]")
print("***take times to run")
parameters = {'max_features':('auto', 'sqrt', 2),
				'max_depth':[i for i in range(60, int(features_dummies.shape[1]/2))],
				'min_samples_split':[i for i in range(4,10)],}
clf = DecisionTreeClassifier(random_state=0)
clf = GridSearchCV(clf, parameters, cv=5)
clf.fit(features_dummies, survived)

clf_randomforest = RandomForestClassifier(n_estimators = 10)
clf_randomforest.set_params(max_features = clf.best_params_['max_features'],
							max_depth = clf.best_params_['max_depth'],
							min_samples_split = clf.best_params_['min_samples_split'])
clf_randomforest.fit(features_dummies, survived)

print("Training accuracy:\t{0}".format(clf_randomforest.score(features_dummies, survived)))
score = cross_val_score(clf_randomforest, features_dummies, survived, cv=5)
print("5-Fold CV accuracy:\t{0}".format(score))
print("Average CV accuracy:\t{0}".format(sum(score)/len(score)))

# export graph
export_graphviz(clf_randomforest.estimators_[0], out_file='problem2.dot',
					feature_names = features_dummies.columns.values,
					class_names = 'Survived')